
#+begin_quote
A Bush aide ([[https://www.huffingtonpost.com/dr-milton-mankoff/the-reality-based-community-and-trumps_b_13351438.html][later identified]] as key adviser Karl Rove) similarly disparaged evidence-based reality, though in his
case by favoring facts created not through faith but power. As he so resonantly explained to those stuck “in what
we call the reality-based community”:

“That’s not the way the world really works anymore. We’re an empire now, and when we act, we create our own
reality. And while you’re studying that reality — judiciously, as you will — we’ll act again, creating other new
realities, which you can study too, and that’s how things will sort out. We’re history’s actors… and you, all of you,
will be left to just study what we do.”

Everything Is Possible and Nothing Is True

Not surprisingly, among its critics Donald Trump’s presidency has [[https://www.newyorker.com/news/our-columnists/why-the-russian-influence-campaign-remains-so-hard-to-understand][inspired]] any [[https://www.weeklystandard.com/charles-j-sykes/when-everything-is-possible-and-nothing-is-true][number]] of [[https://www.amazon.com/dp/0571338526/ref=nosim/?tag=tomdispatch-20][references]] to political
philosopher Hannah Arendt’s description of the dismantling of truth by authoritarian regimes of the previous
century. In her 1951 book, [[https://www.amazon.com/dp/0156701537/ref=nosim/?tag=tomdispatch-20][The Origins of Totalitarianism]], Arendt described the process this way:

“In an ever-changing, incomprehensible world the masses had reached the point where they would, at the same
time, believe everything and nothing, think that everything was possible and that nothing was true… Mass
propaganda discovered that its audience was ready at all times to believe the worst, no matter how absurd, and did
not particularly object to being deceived because it held every statement to be a lie anyhow.”

--- Rebeccan Gordon quoting Karl Rove [fn:TomDispatch-RGordon-PowerFacts]
#+end_quote

[fn:TomDispatch-RGordon-PowerFacts]

#+INCLUDE: ../sources/TomDispatch-RebeccaGordon-Incredibility-facts.org
