
#+begin_quote
Each new generation, every new human being, as he becomes conscious of being inserted between an infinite past and an infinite future, must discover and ploddingly pave anew the path of thought. And it is after all possible, and seems to me likely, that the strange survival of great works, their relative permanence throughout thousands of years, is due to their having been born in the small, inconspicuous track of non-time which their authors’ thought had beaten between an infinite past and an infinite future by accepting past and future as directed, aimed, as it were, at themselves—as their predecessors and successors, their past and their future—thus establishing a present for themselves, a kind of timeless time in which men are able to create timeless works with which to transcend their own finiteness.

--- Hannah Arendt [fn:Arendt-Mind-Life-timeless-great-works]
#+end_quote


[fn:Arendt-Mind-Life-timeless-great-works]

#+INCLUDE: ../sources/Hannah-Arendt-Life-of-The-Mind.org
